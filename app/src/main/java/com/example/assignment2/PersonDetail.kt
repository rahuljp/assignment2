package com.example.assignment2

import java.io.Serializable

data class PersonDetail(var name : String,var address : String , var phone : String, var age : Int) : Serializable
