package com.example.assignment2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.BoringLayout
import kotlinx.android.synthetic.main.activity_registration.*

class registration_Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)


    }

    override fun onStart() {
        super.onStart()
        init()
    }

    private fun init(){
        submit.setOnClickListener{
            if (validate()){
                var person=PersonDetail(name.text.toString(),address.text.toString(), phone.text.toString(),age.text.toString().toInt())
                var intent= Intent(this,DisplayActivity::class.java)
                intent.putExtra("data",person)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out)
            }
        }
    }

    private fun validate() :Boolean {

        var success:Boolean = true
        if (name.text.toString().equals(ignoreCase = true,other=""))
        {
                name.setError("Empty !!!")
                success=false
        }

        if (address.text.toString().equals(ignoreCase = true,other=""))
        {
            address.setError("Empty !!!")
            success=false
        }
        if (phone.text.toString().equals(ignoreCase = true,other=""))
        {
            phone.setError("Empty !!!")
            success=false
        }
        if (age.text.toString().equals(ignoreCase = true,other=""))
        {
            age.setError("Empty !!!")
            success=false
        }

        return success

    }

}